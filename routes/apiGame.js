const express = require('express')
const router = express.Router()
const gameResultController = require('../controllers/apiGameController')
const leaderboard = require('../controllers/leaderboardController')
const restrict = require('../middleware/restrictAndError')

router.post('/game-result', restrict, gameResultController.handleGameResult)
router.get('/leaderboard', leaderboard.handleLeaderboard)
router.get('/player-logs', restrict, gameResultController.playerHistory)

module.exports = router
