const router = require('express').Router()
const auth = require('../controllers/authController')
const restrict = require('../middleware/restrictAndError')

router.get('/', (req, res) => res.status(200).json({ status: 'OK' }))

// router.get("/register", (req, res) => res.render('register'))
router.post('/register', auth.register)

// router.get("/login", (req, res) => res.render('login'))
router.post('/login', auth.login)

router.get('/whoami', restrict, auth.whoami)

//dari FE ke BE
router.post('/refresh-token', auth.refreshToken)

router.put('/edit-player', restrict, auth.updatePlayer)

module.exports = router
