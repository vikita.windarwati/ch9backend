var express = require('express')
var router = express.Router()

const Players = require('../models').Player
const PlayerGame = require('../models').PlayerGame
const TotalScore = require('../models').TotalScore

/* TEST JSON SEQUELIZE PLAYER */

// router.get('/', (req, res)=> {
//   Players.findAll({
//         include: [{
//           model: PlayerGame,
//           as: 'PlayerGame',
//         },
//       ],
//     })
//     .then((player)=> {res.status(200).send(player)})
//     .catch((error) => { res.status(400).send(error);})
// });

/* TEST JSON SEQUELIZE TotalScore */

router.get('/total-score', (req, res) => {
  TotalScore.findAll()
    .then((score) => {
      res.status(200).send(score)
    })
    .catch((error) => {
      res.status(400).send(error)
    })
})

router.get('/players', (req, res) => {
  Players.findAll()
    .then((score) => {
      res.status(200).send(score)
    })
    .catch((error) => {
      res.status(400).send(error)
    })
})

router.get('/player-games', (req, res) => {
  PlayerGame.findAll()
    .then((score) => {
      res.status(200).send(score)
    })
    .catch((error) => {
      res.status(400).send(error)
    })
})



module.exports = router
