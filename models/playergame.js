'use strict'
const { Model } = require('sequelize')
module.exports = (sequelize, DataTypes) => {
  class PlayerGame extends Model {
    static associate(models) {
      this.belongsTo(models.Player, {
        foreignKey: 'user_id',
        as: 'Player',
      })
    }
  }
  PlayerGame.init(
    {
      user_id: DataTypes.INTEGER,
      game: DataTypes.STRING,
      score: DataTypes.INTEGER,
    },
    {
      sequelize,
      updatedAt: false,
      modelName: 'PlayerGame',
    }
  )
  return PlayerGame
}
