var express = require('express')
var path = require('path')
var cookieParser = require('cookie-parser')
var logger = require('morgan')
var cors = require('cors')

var indexRouter = require('./routes/index')
var usersRouter = require('./routes/users')
const authRoutes = require('./routes/authRoutes')
const apiGameRouter = require('./routes/apiGame')
const passport = require('./middleware/passportJWT')
const handleError = require('./middleware/hanldeError')

var app = express()

app.use(logger('dev'))
app.use(express.json())
app.use(express.urlencoded({ extended: false }))
app.use(cookieParser())
app.use(express.static(path.join(__dirname, 'public')))
app.use(cors()) // cors
app.use(passport.initialize())

// app.set('view engine', 'ejs')

app.use('/', indexRouter)
app.use('/', authRoutes)
app.use('/users', usersRouter)
app.use('/api', apiGameRouter)

//error handling
app.use(handleError.errorHandler)
app.use(handleError.unknownEndpoint)

module.exports = app
