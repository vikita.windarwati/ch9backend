const passport = require ('passport' );
const { Strategy : JwtStrategy, ExtractJwt } = require ('passport-jwt');
require('dotenv').config();
const Player = require("../models").Player;

const options = {
    jwtFromRequest : ExtractJwt.fromHeader ("authorization"),
    secretOrKey : process.env.ACCESS_TOKEN_SECRET,
    passReqToCallback: true,
}

passport.use(new JwtStrategy(options, async(req, payload, done) => {

    Player.findByPk(payload.id)
        .then(user =>{
            if(user) {
                req.user = user;
                done(null, user)
            }
        })
        .catch(err => done(err, false))
}))

module.exports = passport