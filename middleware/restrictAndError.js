const express = require('express');
const router = express.Router();
const passport = require("./passportJWT");
const _ = require('lodash');



router.all('*', (req, res, next) => {
  passport.authenticate('jwt', { session: false }, (err, user, info) => {
    if (err || !user || _.isEmpty(user)) {
      return next(info);
    // PASS THE ERROR OBJECT TO THE NEXT ROUTE i.e THE APP'S COMMON ERROR HANDLING MIDDLEWARE
    } 
    // else if (user) {
    //   //how to send this user to controller whoami
    //   returnuserData.push(user)

    // } 
    else {
        return next();
    }
  })(req, res, next);
});

module.exports = router